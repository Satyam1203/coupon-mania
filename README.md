# coupon-generator

An online hiring assignment
Live @ https://coupon-mania.herokuapp.com/

# Requisites:

- Node.js (version 10+)
- mongodb

# Steps to run:

- Clone the repository `git clone https://gitlab.com/Satyam1203/coupon-mania.git`
- Add a .env file to root folder and add these lines.
  ```
  PORT=5000
  MONGO_URI=mongodb://localhost:27017/coupon-app
  ```
- Run `npm install` in project root and /client folder.
- Run `npm run dev` from project root.
- The project should be running at `http://localhost:3000`
