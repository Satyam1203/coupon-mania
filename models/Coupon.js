const mongoose = require("mongoose");

const couponSchema = new mongoose.Schema(
  {
    code: {
      type: String,
      required: true,
    },
    minValue: {
      type: Number,
      required: true,
    },
    type: {
      type: String,
      enum: ["flat", "percentage"],
      required: true,
    },
    discount: {
      type: Number,
      required: true,
    },
    maxDiscount: {
      type: Number,
    },
    validFrom: {
      type: Date,
      required: true,
    },
    validTill: {
      type: Date,
      required: true,
    },
    deleted: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Coupon", couponSchema);
