import "./App.css";
import "./styles/style.css";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";

import Navbar from "./components/Navbar";
import Create from "./components/Create";
import List from "./components/List";
import Checkout from "./components/Checkout";
import Home from "./components/Home";

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar />
        <Switch>
          <Route path="/create">
            <Create />
          </Route>
          <Route path="/list">
            <List />
          </Route>
          <Route path="/apply">
            <Checkout />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
