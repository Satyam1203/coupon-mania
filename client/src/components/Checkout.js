import { useState } from "react";
import axios from "axios";

function Checkout() {
  const [amount, setAmount] = useState("");
  const [code, setCode] = useState("");
  const [discount, setDiscount] = useState("");
  const [status, setStatus] = useState("");

  const applyCoupon = () => {
    if (!amount) {
      setStatus("Please enter a valid amount");
      return;
    }
    setStatus("");

    axios("/api/apply", {
      method: "POST",
      data: { amount, code },
    })
      .then((res) => {
        if (res.data?.success) {
          setDiscount(res.data.discountAmount);
          setStatus("");
          console.log("if");
        } else {
          setStatus(res.data.message);
          console.log("else");
        }
        console.log(res.data);
      })
      .catch((e) => {
        console.log(e.message);
        setStatus("Request failed. Please try again.");
      });
  };

  return (
    <main className="main-container">
      <h2>Checkout</h2>
      <div className="cart-checkout">
        <div className="cart-cols">
          <span>Your cart value</span>
          <input
            type="number"
            placeholder="5000"
            value={amount}
            onChange={(e) => setAmount(e.target.value)}
          />
        </div>
        <div className="code-input-wrapper">
          <input
            type="text"
            placeholder="Enter coupon code"
            value={code}
            onChange={(e) => setCode(e.target.value)}
          />
          <button onClick={applyCoupon}>Apply</button>
        </div>
        {discount !== "" && (
          <>
            <div className="cart-cols">
              <span>Discount</span>
              <span>- ${discount}</span>
            </div>
            <div className="cart-cols">
              <span>Net amount</span>
              <span>${amount - discount}</span>
            </div>
          </>
        )}
      </div>
      <p>{status}</p>
    </main>
  );
}

export default Checkout;
