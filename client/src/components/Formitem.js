import React from "react";

function Formitem({ field, options, ...props }) {
  return (
    <div className="input-wrapper">
      <div>{field}</div>
      {props.type === "radio" ? (
        <div className="input-radio-items">
          {options.map((option) => (
            <label key={option}>
              <input
                type="text"
                required
                {...props}
                value={option}
                defaultChecked={option === "flat"}
              />
              {option}
            </label>
          ))}
        </div>
      ) : (
        <input type="text" required {...props} />
      )}
    </div>
  );
}

export default Formitem;
