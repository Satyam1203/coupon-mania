import svg from "../payments.svg";

function Home() {
  return (
    <div className="home-page">
      <img src={svg} alt="payments" />
      <h1>Create, view and apply coupons.</h1>
    </div>
  );
}

export default Home;
