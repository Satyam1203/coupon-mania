import { Link } from "react-router-dom";

function Navbar() {
  return (
    <div className="navigation-bar">
      <h2 className="app-title">
        <Link to="/">Coupon-mania</Link>
      </h2>
      <nav>
        <Link to="/create">Create</Link>
        <Link to="/list">List</Link>
        <Link to="/apply">Go to cart</Link>
      </nav>
    </div>
  );
}

export default Navbar;
