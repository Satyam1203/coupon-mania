import { useState } from "react";
import axios from "axios";
import Formitem from "./Formitem";

const initialFormState = {
  minValue: "",
  type: "flat",
  discount: "",
  maxDiscount: "",
  validFrom: "",
  validTill: "",
};

function Create() {
  const [state, setState] = useState(initialFormState);
  const [status, setStatus] = useState("");

  const createCoupon = (e) => {
    e.preventDefault();
    setStatus("");

    axios("/api/create", {
      method: "POST",
      data: state,
      "Content-Type": "application/json",
    })
      .then((res) => {
        console.log(res.data);
        if (res.data.success) {
          setState(initialFormState);
          setStatus(`Coupon create with code ${res.data.coupon.code}`);
        } else {
          setStatus(res.data.message);
        }
      })
      .catch((e) => console.log(e.message));
  };

  return (
    <main className="form-wrapper">
      <h2>Create a new Coupon</h2>
      <form id="create-coupon-form" onSubmit={createCoupon}>
        <Formitem
          field="Minimum Cart value"
          type="number"
          value={state.minValue}
          onChange={(e) =>
            setState((s) => ({ ...s, minValue: e.target.value }))
          }
          placeholder={500}
        />
        <Formitem
          field="Coupon Type"
          type="radio"
          name="type"
          options={["flat", "percentage"]}
          onChange={(e) => setState((s) => ({ ...s, type: e.target.value }))}
        />
        <Formitem
          field={`Discount (${state.type === "flat" ? "amount" : "percent"})`}
          type="number"
          value={state.discount}
          onChange={(e) =>
            setState((s) => ({ ...s, discount: e.target.value }))
          }
          placeholder={20}
        />
        {state.type === "percentage" && (
          <Formitem
            field="Maximum Discount"
            type="number"
            value={state.maxDiscount}
            onChange={(e) =>
              setState((s) => ({ ...s, maxDiscount: e.target.value }))
            }
            placeholder={100}
          />
        )}
        <Formitem
          field="Valid from"
          type="date"
          value={state.validFrom}
          onChange={(e) =>
            setState((s) => ({ ...s, validFrom: e.target.value }))
          }
        />
        <Formitem
          field="Valid till"
          type="date"
          value={state.validTill}
          min={
            !state.validFrom
              ? new Date().toISOString().split("T")[0]
              : state.validFrom
          }
          onChange={(e) =>
            setState((s) => ({ ...s, validTill: e.target.value }))
          }
        />
        <button type="submit">Create </button>
      </form>
      <p>{status}</p>
    </main>
  );
}

export default Create;
