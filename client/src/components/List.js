import { useState, useEffect } from "react";
import axios from "axios";

function List() {
  const [coupons, setCoupons] = useState([]);
  const [status, setStatus] = useState("");

  useEffect(() => {
    axios("/api/get", {
      method: "GET",
    })
      .then((res) => {
        if (res.data.success) {
          setCoupons(res.data.coupons);
          setStatus("");
        } else {
          setCoupons([]);
          setStatus(res.data.message);
        }
      })
      .catch((e) => {
        console.log(e.message);
        setStatus("Request failed. Please try again.");
      });
  }, []);

  return (
    <main className="main-container">
      <h2>All Coupons</h2>
      {coupons.length > 0 && (
        <div className="table">
          <table cellSpacing={0} cellPadding={10}>
            <thead>
              <tr>
                <th>Code</th>
                <th>Min. cart value</th>
                <th>Type</th>
                <th>Discount</th>
                <th>Valid From</th>
                <th>Valid Till</th>
              </tr>
            </thead>
            <tbody>
              {coupons.map((coupon) => (
                <tr key={coupon._id}>
                  <td>{coupon.code}</td>
                  <td>$ {coupon.minValue}</td>
                  <td>{coupon.type}</td>
                  <td>
                    {coupon.type === "flat" && "$"}
                    {coupon.discount}
                    {coupon.type === "percentage" &&
                      `% (max $${coupon.maxDiscount})`}
                  </td>
                  <td>{coupon.validFrom.slice(0, 10)}</td>
                  <td>{coupon.validTill.slice(0, 10)}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )}
      {coupons.length === 0 && <p>{status}</p>}
    </main>
  );
}

export default List;
