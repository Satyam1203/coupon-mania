const router = require("express").Router();
const Coupon = require("./models/Coupon");

router.route("/create").post(async (req, res) => {
  try {
    const code = Math.random().toString(36).slice(2).toUpperCase();
    const coupon = await Coupon.create({ ...req.body, code });
    if (coupon) {
      res.json({ success: true, message: "Coupon created", coupon });
    } else {
      res.json({
        success: false,
        message: "Unable to create now. Please try again.",
      });
    }
  } catch (e) {
    res.json({ error: e.message });
  }
});

router.route("/get").get(async (req, res) => {
  try {
    const coupons = await Coupon.find({ deleted: false });
    res.json({ success: true, coupons });
  } catch (e) {
    res.json({ error: e.message });
  }
});

router.route("/delete").post(async (req, res) => {
  try {
    const updateCoupon = await Coupon.updateOne(
      { _id: req.body.id },
      { deleted: true }
    );
    if (updateCoupon.modifiedCount) {
      res.json({ updated: true, message: "Coupon deleted" });
    } else {
      res.json({ updated: false, message: "Couldn't delete now" });
    }
  } catch (e) {
    res.json({ error: e.message });
  }
});

router.route("/apply").post(async (req, res) => {
  try {
    const coupon = await Coupon.findOne({ code: req.body.code });
    if (!coupon || new Date(coupon.validFrom) > Date.now()) {
      res.json({ isValid: false, message: "Coupon code is invalid" });
    } else if (new Date(coupon.validTill) < Date.now()) {
      res.json({ isValid: true, message: "Coupon code is expired" });
    } else if (coupon.minValue > req.body.amount) {
      res.json({
        isValid: true,
        message: `You need to add minimum $${coupon.minValue} worth of items to apply this coupon`,
      });
    } else {
      const discountAmount =
        coupon.type === "flat"
          ? coupon.discount
          : Math.min(
              coupon.maxDiscount,
              coupon.discount * req.body.amount * 0.01
            );
      res.json({
        success: true,
        isValid: true,
        discountAmount,
        message: `Coupon applied. Hooray!!`,
      });
    }
  } catch (e) {
    res.json({ error: e.message });
  }
});

module.exports = router;
